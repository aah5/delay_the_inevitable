defmodule DelayTheInevitableWeb.UserController do
  use DelayTheInevitableWeb, :controller

  action_fallback DelayTheInevitableWeb.FallbackController

  def create(conn, data) do
    uid = data["uid"]

    Phoenix.PubSub.broadcast(
      DelayTheInevitable.PubSub,
      "user:#{uid}",
      {:connected, data}
    )

    send_resp(conn, :no_content, "")
  end
end
