defmodule DelayTheInevitableWeb.PageController do
  use DelayTheInevitableWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
