defmodule DelayTheInevitableWeb.UserView do
  use DelayTheInevitableWeb, :view
  alias DelayTheInevitableWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      uid: user.uid,
      utoken: user.utoken,
      domain: user.domain,
      httpPort: user.httpPort,
      wsPort: user.wsPort,
      httpsPort: user.httpsPort,
      wssPort: user.wssPort,
      platform: user.platform,
      appVersion: user.appVersion,
      version: user.version
    }
  end
end
