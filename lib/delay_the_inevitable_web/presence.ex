defmodule DelayTheInevitableWeb.Presence do
  use Phoenix.Presence,
    otp_app: :delay_the_inevitable,
    pubsub_server: DelayTheInevitable.PubSub
end
