defmodule DelayTheInevitableWeb.HomeLive do
  use DelayTheInevitableWeb, :live_view
  use Phoenix.Component

  alias DelayTheInevitableWeb.Router.Helpers, as: Routes
  alias DelayTheInevitableWeb.Endpoint
  alias DelayTheInevitableWeb.{GameLive, MainMenuLive}

  def render(assigns) do
    ~H"""
    <h1>CoolQuiz</h1>
    <p>This is a quiz app with integration to lovense devices.
    Source code: <a href="https://gitlab.com/aah5/delay_the_inevitable">https://gitlab.com/aah5/delay_the_inevitable</a>
    </p>

    <div style="display: flex; flex-direction: column">
      <h2>How to connect:</h2>
      <ol>
        <li>Install Lovense Connect app on your phone</li>
        <li>Scan the QR code below with it</li>
        <li>You'll be automatically forwarded to the next page</li>
      </ol>
      
      <h2>QR Code:</h2>
      <img src={@qr} style="max-width: 400px" />
      <%= @code %>
    </div>
    """
  end

  def mount(params, session, socket) do
    if connected?(socket) do
      uid = UUID.uuid4() <> "@del.cwiki.cyou"
      Phoenix.PubSub.subscribe(DelayTheInevitable.PubSub, "user:#{uid}")

      # {:ok, assign(socket, :chats, EchatEngine.get_all_chats())}
      token = Application.fetch_env!(:delay_the_inevitable, :lovense_token)

      response =
        HTTPoison.post!(
          "https://api.lovense.com/api/lan/getQrCode",
          Jason.encode!(%{token: token, uid: uid, uname: "Anonymous", v: 2}),
          [{"content-type", "application/json"}],
          hackney: [:insecure]
        )

      data = Jason.decode!(response.body)["data"]

      socket =
        if Map.has_key?(params, "game_id") do
          assign(socket, game_id: params["game_id"])
        else
          socket
        end

      {:ok, assign(socket, uid: uid, code: data["code"], qr: data["qr"])}
    else
      {:ok, assign(socket, uid: nil, code: "", qr: "")}
    end
  end

  def handle_info({:connected, user}, socket) do
    IO.puts("Connected!")

    if Map.has_key?(socket.assigns, :game_id) do
      game_id = socket.assigns.game_id

      {:noreply,
       push_redirect(socket, to: Routes.live_path(socket, GameLive, game_id, user: user))}
    else
      {:noreply, push_redirect(socket, to: Routes.live_path(socket, MainMenuLive, %{user: user}))}
    end
  end

  def handle_info(_msg, socket) do
    {:noreply, socket}
  end
end
