defmodule DelayTheInevitableWeb.GameLive do
  use DelayTheInevitableWeb, :live_view
  use Phoenix.Component

  alias DelayTheInevitableWeb.Router.Helpers, as: Routes
  alias DelayTheInevitableWeb.Endpoint
  alias DelayTheInevitableWeb.HomeLive

  alias DelayTheInevitableWeb.Presence
  alias DelayTheInevitableWeb.GameLive

  alias DelayTheInevitable.PubSub

  alias DelayTheInevitable.Game
  alias DelayTheInevitable.HornyAPI

  @start_time 120

  def render(assigns) do
    ~H"""
    <div class="header">
      <%= if @started do %>
      <div style="position: fixed; right: 1em; top: 1em; max-width: 15em; display: flex; flex-direction: column">
        <div><b>Level: </b><%= @level %></div>
        <div><b>Time playing: </b><%= Time.diff(Time.utc_now, @start_time) %> seconds</div>
      </div>
      <% end %>
      <div style="position: fixed; left: 1em; top: 1em; max-width: 15em; display: flex; flex-direction: column">
        <div><b>Number of players: </b><%= Enum.count(@users) %></div>
        <b>Players:</b>
        <ol>
          <%= for {name, user} <- @users do %>
            <li><%= "#{String.slice(name, 0, 5)}: #{user.score}" %></li>   
          <% end %>
        </ol>
      </div>
      <button phx-click="start_game">Start</button>
      (link share:
        <a href={Routes.live_path(Endpoint, GameLive, @id)}>
          <%= @id %>
        </a>
      )
    </div>
    <div class="content">
      <div class="time" style="font-size: xx-large">Time left:
        <%= @time_left %> seconds
    </div>
    <div class="question">
      <%= if @started do %>
        <h3>Question:</h3>
          <p style="font-size: xx-large"><%= @question.question %></p>
    <.form let={f} for={:answer} phx-submit="submit_answer">
    Answer: <%= text_input f, :answer %>
    </.form>
      <% end %>
    </div>
    </div>

    """
  end

  def mount(%{"id" => id, "user" => user}, _session, socket) do
    IO.inspect(id)

    presence = "game:#{id}"
    uid = user["uid"]

    if connected?(socket) do
      {:ok, _} =
        Presence.track(self(), presence, uid, %{
          # name: user[:name],
          score: 0,
          joined_at: DateTime.utc_now()
        })

      Phoenix.PubSub.subscribe(PubSub, presence)
    end

    Phoenix.PubSub.subscribe(PubSub, "user:#{uid}")

    {:ok,
     assign(socket,
       id: id,
       user: user,
       users: %{},
       started: false,
       score: 0,
       vib_timer: nil
     )
     |> handle_bg_timer(0)
     |> assign_time_left()
     |> handle_joins(Presence.list(presence))}
  end

  def mount(%{"id" => id}, _session, socket) do
    {:ok,
     push_redirect(socket,
       to: Routes.live_path(socket, HomeLive, %{game_id: id})
     )}
  end

  @timer 1000
  def handle_event("start_game", _params, socket) do
    Phoenix.PubSub.broadcast(
      PubSub,
      "game:#{socket.assigns.id}",
      :start_game
    )

    {:noreply, start_game(socket)}
  end

  def handle_event("submit_answer", %{"answer" => %{"answer" => answer}}, socket) do
    socket =
      if answer == socket.assigns.question.answer do
        time = HornyAPI.vib_good(socket.assigns.user["uid"])

        socket
        |> extend_time()
        |> increase_score()
        |> new_level()
        |> new_question()
        |> handle_bg_timer(time)
      else
        time = HornyAPI.vib_bad(socket.assigns.user["uid"])

        handle_bg_timer(socket, time)
      end

    {:noreply, socket}
  end

  defp extend_time(socket) do
    level = socket.assigns.level
    increase = cond do
      level == 0 -> 2
      level == 1 -> 4
      level == 2 -> 7
      level == 3 -> 10
      true ->
        socket.assigns.level * 3 + 3
    end

    update(socket, :time_left, fn x ->
      x + increase
    end)
  end

  defp increase_score(socket) do
    new_score = socket.assigns.score + Enum.random(1..(socket.assigns.level * 10 + 5))

    {:ok, _} =
      Presence.update(
        self(),
        "game:#{socket.assigns.id}",
        socket.assigns.user["uid"],
        fn x ->
          %{x | score: new_score}
        end
      )

    assign(socket, score: new_score)
  end

  defp new_level(socket) do
    level =
      socket.assigns.users
      |> Enum.map(fn {_, v} -> v.score end)
      |> Enum.sum()

    score = level / Enum.count(socket.assigns.users)
    level = cond do
      score < 100 -> 0
      score < 500 -> 1
      score < 1500 -> 2
      score < 2000 -> 3
      true ->
        Kernel.trunc(score / 2000) + 3
    end
    
    #level = Kernel.trunc(score / 100)
    assign(socket, level: level)
  end

  defp new_question(socket) do
    assign(socket, question: Game.new_question(socket.assigns.level))
  end

  defp start_game(socket) do
    if !socket.assigns.started do
      Process.send_after(self(), :tick, @timer)

      assign(socket,
        started: true,
        start_time: Time.utc_now(),
        level: 1,
        question: Game.new_question(1)
      )
      |> assign_time_left()
    else
      socket
    end
  end

  def handle_info(:tick, socket) do
    if socket.assigns.started do
      socket = assign_time_left(socket)

      if socket.assigns.time_left > 0 do
        Process.send_after(self(), :tick, @timer)
      end

      {:noreply, assign_time_left(socket)}
    else
      {:noreply, socket}
    end
  end

  def handle_info(:play_bg, socket) do
    Process.cancel_timer(socket.assigns.vib_timer)
    time = HornyAPI.vib_bg(socket.assigns.user["uid"], socket.assigns.score)

    {:noreply, handle_bg_timer(socket, time)}
  end

  def handle_info(%Phoenix.Socket.Broadcast{event: "presence_diff", payload: diff}, socket) do
    time = HornyAPI.vib_good_team(socket.assigns.user["uid"])

    {
      :noreply,
      socket
      |> handle_leaves(diff.leaves)
      |> handle_joins(diff.joins)
      |> handle_bg_timer(time)
    }
  end

  def handle_info(:start_game, socket) do
    time = HornyAPI.vib_bad(socket.assigns.user["uid"])

    {:noreply,
     socket
     |> start_game()
     |> handle_bg_timer(time)}
  end

  def handle_info(_msg, socket) do
    {:noreply, socket}
  end

  defp handle_bg_timer(socket, time) do
    timer = Process.send_after(self(), :play_bg, time)
    assign(socket, vib_timer: timer)
  end

  defp handle_joins(socket, joins) do
    Enum.reduce(joins, socket, fn {user, %{metas: [meta | _]}}, socket ->
      assign(socket, :users, Map.put(socket.assigns.users, user, meta))
    end)
  end

  defp handle_leaves(socket, leaves) do
    Enum.reduce(leaves, socket, fn {user, _}, socket ->
      assign(socket, :users, Map.delete(socket.assigns.users, user))
    end)
  end

  defp assign_time_left(socket) do
    assigns = socket.assigns

    time_left =
      if assigns.started do
        max(socket.assigns.time_left - 1, 0)
      else
        @start_time
      end

    assign(socket, time_left: time_left)
  end
end
