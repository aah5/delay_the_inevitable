defmodule DelayTheInevitableWeb.MainMenuLive do
  use DelayTheInevitableWeb, :live_view
  use Phoenix.Component

  alias DelayTheInevitableWeb.Router.Helpers, as: Routes
  alias DelayTheInevitableWeb.Endpoint
  alias DelayTheInevitableWeb.HomeLive

  alias DelayTheInevitableWeb.GameLive

  def render(assigns) do
    ~H"""
    <button phx-click="new_game">Start new game</button>
    <h3>Debug Input Test</h3>
    <button phx-click="vib_good">Vibrate Good</button>
    <button phx-click="vib_bad">Vibrate Bad</button>

    <h3>Connection Debug Info:</h3>
    <div>
      <%= inspect(@user) %>
    </div>
    """
  end

  def mount(%{"user" => user}, _session, socket) do
    uid = user["uid"]
    Phoenix.PubSub.subscribe(DelayTheInevitable.PubSub, "user:#{uid}")
    {:ok, assign(socket, user: user)}
  end

  def mount(_params, _session, socket) do
    {:ok,
     push_redirect(socket,
       to: Routes.live_path(socket, HomeLive)
     )}
  end

  def handle_event("new_game", _params, socket) do
    id = UUID.uuid4()

    {:noreply,
     push_redirect(socket, to: Routes.live_path(socket, GameLive, id, user: socket.assigns.user))}
  end

  def handle_event("vib_good", _params, socket) do
    IO.puts("vib_good")
    token = Application.fetch_env!(:delay_the_inevitable, :lovense_token)
    uid = socket.assigns.user["uid"]

    response =
      HTTPoison.post!(
        "https://api.lovense.com/api/lan/v2/command",
        Jason.encode!(%{
          token: token,
          uid: uid,
          command: "Function",
          action: "Vibrate:16",
          timeSec: 20,
          loopRunningSec: 9,
          loopPauseSec: 4,
          apiVer: 1
        }),
        [{"content-type", "application/json"}],
        hackney: [:insecure]
      )

    IO.inspect(response.body)

    {:noreply, socket}
  end

  def handle_event("vib_bad", _params, socket) do
    {:noreply, socket}
  end

  def handle_info(_msg, socket) do
    {:noreply, socket}
  end
end
