defmodule DelayTheInevitable.HornyAPI do
  @url "https://api.lovense.com/api/lan/v2/command"

  def send_command(command) do
    with %{"code" => 200} <-
           HTTPoison.post!(
             @url,
             Jason.encode!(format_command(command)),
             [{"content-type", "application/json"}],
             hackney: [:insecure]
           ).body
           |> Jason.decode!() do
      :ok
    else
      x -> x
    end
  end

  defp format_command(command) do
    token = Application.fetch_env!(:delay_the_inevitable, :lovense_token)

    command
    |> Map.put(:apiVer, 1)
    |> Map.put(:token, token)
  end

  def vib_ex(uid) do
    IO.puts("vib_ex")

    send_command(%{
      uid: uid,
      command: "Function",
      action: "Vibrate:16",
      timeSec: 20,
      loopRunningSec: 9,
      loopPauseSec: 4
    })

    20
  end

  def vib_good(uid) do
    IO.puts("vib_good")

    send_command(%{
      uid: uid,
      command: "Function",
      action: "Vibrate:16",
      timeSec: 5
    })

    5
  end

  def vib_good_team(uid) do
    IO.puts("vib_good_team")

    send_command(%{
      uid: uid,
      command: "Function",
      action: "Vibrate:13",
      timeSec: 3
    })

    3
  end

  def vib_bad(uid) do
    IO.puts("vib_bad")

    send_command(%{
      uid: uid,
      command: "Function",
      action: "Vibrate:20",
      timeSec: 3
    })

    3
  end

  def vib_bad_team(uid) do
    IO.puts("vib_bad_team")

    send_command(%{
      uid: uid,
      command: "Function",
      action: "Vibrate:20",
      timeSec: 1
    })

    1
  end

  # def vib_bg(uid, freq) when is_integer(freq) and freq >= 100 and freq <= 1000 do
  # def vib_bg(uid, freq) when is_integer(freq) and freq >= 100 and freq <= 1000 do
  def vib_bg(uid, score) do
    # IO.puts "vib_bg #{score}"
    freq =
      if score >= 200 do
        100
      else
        300 - score
      end

    strength =
      cond do
        score >= 200 ->
          "20;19;18;17;13;12;12;11;11;1;0;1;11;11;12;12;13;17;18;19;"

        score >= 150 ->
          "15;14;13;12;11;10;5;5;3;1;0;1;3;5;5;10;11;12;13;14;"

        score >= 100 ->
          "12;11;10;7;4;3;2;2;1;0;0;0;1;2;2;3;4;7;10;11;"

        true ->
          "10;9;8;7;3;2;2;1;1;0;0;0;1;1;2;2;3;7;8;9;"
      end

    send_command(%{
      uid: uid,
      command: "Pattern",
      rule: "V:1;F:v;S:#{freq}#",
      strength: strength,
      timeSec: freq * 20
    })

    freq * 20
  end
end
