defmodule DelayTheInevitable.Game do
  def new_question(level) when is_integer(level) and level < 10 do
    x = Enum.random(2..(3+Kernel.trunc(:math.pow(10, level))))
    y = Enum.random(2..(3+Kernel.trunc(:math.pow(10, level - 1))))

    answer = "#{x * y}"

    %{question: "#{x}*#{y}=?", answer: answer}
  end

  def new_question(level) when is_integer(level) do
    a = Enum.random(1..Kernel.trunc(:math.pow(10, level - 1)))
    b = Enum.random(1..Kernel.trunc(:math.pow(10, level - 2)))

    # (x - a)(x - b) = x^2 - (a+b)x + a*b

    an = min(a,b)
    b = max(a,b)
    a = an

    answer = "#{a},#{b}"

    %{question: "(x-a)(x-b)=x^2+#{a+b}x+#{a*b}, what is a,b if a<=b?", answer: answer}
  end

end
