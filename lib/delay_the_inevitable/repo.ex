defmodule DelayTheInevitable.Repo do
  use Ecto.Repo,
    otp_app: :delay_the_inevitable,
    adapter: Ecto.Adapters.Postgres
end
