defmodule DelayTheInevitable.Mailer do
  use Swoosh.Mailer, otp_app: :delay_the_inevitable
end
