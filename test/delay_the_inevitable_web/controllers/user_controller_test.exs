defmodule DelayTheInevitableWeb.UserControllerTest do
  use DelayTheInevitableWeb.ConnCase

  import DelayTheInevitable.AccountsFixtures

  alias DelayTheInevitable.Accounts.User

  @create_attrs %{
    appVersion: "some appVersion",
    domain: "some domain",
    httpPort: 42,
    httpsPort: 42,
    platform: "some platform",
    uid: "some uid",
    utoken: "some utoken",
    version: 42,
    wsPort: 42,
    wssPort: 42
  }
  @update_attrs %{
    appVersion: "some updated appVersion",
    domain: "some updated domain",
    httpPort: 43,
    httpsPort: 43,
    platform: "some updated platform",
    uid: "some updated uid",
    utoken: "some updated utoken",
    version: 43,
    wsPort: 43,
    wssPort: 43
  }
  @invalid_attrs %{
    appVersion: nil,
    domain: nil,
    httpPort: nil,
    httpsPort: nil,
    platform: nil,
    uid: nil,
    utoken: nil,
    version: nil,
    wsPort: nil,
    wssPort: nil
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      conn = get(conn, Routes.user_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create user" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.user_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "appVersion" => "some appVersion",
               "domain" => "some domain",
               "httpPort" => 42,
               "httpsPort" => 42,
               "platform" => "some platform",
               "uid" => "some uid",
               "utoken" => "some utoken",
               "version" => 42,
               "wsPort" => 42,
               "wssPort" => 42
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update user" do
    setup [:create_user]

    test "renders user when data is valid", %{conn: conn, user: %User{id: id} = user} do
      conn = put(conn, Routes.user_path(conn, :update, user), user: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.user_path(conn, :show, id))

      assert %{
               "id" => ^id,
               "appVersion" => "some updated appVersion",
               "domain" => "some updated domain",
               "httpPort" => 43,
               "httpsPort" => 43,
               "platform" => "some updated platform",
               "uid" => "some updated uid",
               "utoken" => "some updated utoken",
               "version" => 43,
               "wsPort" => 43,
               "wssPort" => 43
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put(conn, Routes.user_path(conn, :update, user), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete user" do
    setup [:create_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete(conn, Routes.user_path(conn, :delete, user))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.user_path(conn, :show, user))
      end
    end
  end

  defp create_user(_) do
    user = user_fixture()
    %{user: user}
  end
end
